from django.db import models
from django.utils import timezone
import urllib.request, json

# Create your models here.
class User(models.Model):
    user_id = models.CharField(max_length=30)
    token = models.CharField(max_length=100)
    expires_in = models.CharField(max_length=30)
    created_date = models.DateTimeField(
        default=timezone.now
    )


    def getUserInfo(self):
        url = 'https://api.vk.com/method/users.get?'
        url += 'user_ids=' + self.user_id
        url += '&v=5.74'

        with urllib.request.urlopen(url) as uri:
            UserInfo = json.loads(uri.read().decode())

        return UserInfo['response'][0]

    def getUserFriends(self, count=5):
        url = 'https://api.vk.com/method/friends.get?'
        url += 'user_id=' + self.user_id
        url += '&order=random'
        url += '&count=' + str(count)
        url += '&fields=nickname'
        url += '&v=5.74'

        with urllib.request.urlopen(url) as uri:
            UserFriends = json.loads(uri.read().decode())

        return UserFriends['response']['items']