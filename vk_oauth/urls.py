from django.urls import path

from . import views

urlpatterns = [
    path('login', views.vk_login, name='vk_login'),
    path('logout', views.vk_logout, name='vk_logout'),
    path('oauth_code', views.vk_oauth_code, name='vk_oauth_code'),
]