from django.shortcuts import render, redirect
from webim_vk.settings import VK_OAUTH_CLIEND_ID, VK_OAUTH_SECRET
from django.http import HttpResponse
import urllib.request, json
from .models import User

# Create your views here.
def vk_login(request):
    url = 'https://oauth.vk.com/authorize?'
    url += 'client_id=' + VK_OAUTH_CLIEND_ID
    url += '&redirect_uri=http://webim.xdx.su/vk/oauth_code'
    url += '&display=popup'
    url += '&scope=2'
    url += '&v=5.74'

    return redirect(url)

def vk_oauth_code(request):
    if request.method == 'GET':

        if request.GET.get('error', False):
            HttpResponse('error: ' + request.GET['error']
                         + 'error_description' + request.GET['error_description'])

        if request.GET.get('code', False):
            url = 'https://oauth.vk.com/access_token?'
            url += 'client_id=' + VK_OAUTH_CLIEND_ID
            url += '&client_secret=' + VK_OAUTH_SECRET
            url += '&code=' + request.GET['code']
            url += '&redirect_uri=http://webim.xdx.su/vk/oauth_code'
            url += '&v=5.74'

            with urllib.request.urlopen(url) as uri:
                data = json.loads(uri.read().decode())

                #TODO if error in process get token ...

                user = User()
                user.user_id = data['user_id']
                user.token = data['access_token']
                user.expires_in = data['expires_in']
                user.save()

                request.session['vk_oauth_user_id'] = user.id

                return redirect('/')

def vk_logout(request):
    #TODO if session is None
    user = User.objects.get(id=request.session['vk_oauth_user_id'])
    user.delete()
    request.session.delete()
    return redirect('/')