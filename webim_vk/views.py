from django.shortcuts import render
from vk_oauth.models import User

def index(request):
    context = {
        'user': False
    }
    if 'vk_oauth_user_id' in request.session:
        user = User.objects.get(id=request.session['vk_oauth_user_id'])
        context = {
            'user': user
        }

    return render(request, 'index.html', context)
